=== Plugin Name ===

* Contributors: benjamin.ninassi@inria.fr, snjazur@gmail.com, thierry.vieville@inria.fr, 
* Website URL: https://cai.inria.fr
* Project URL: https://www.cai.community
* Development pad : https://pad.inria.fr/p/np_D13sp24NPvFr1KLc_cai.inria.fr
* Tags: CAI 
* Requires at least: 3.0.1
* Tested up to: 4.8
* License: CeCILL-C
* License URI: https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html

== Description ==

This plugin integrates the wordpress [https://cai.inria.fr](https://cai.inria.fr) child theme and plugin middleware

== Installation ==

* The CAI functionnality is avalaible as a children theme of [femina](https://fr.wordpress.org/themes/femina).

* In order to activate the CSS, using the femina original theme, insert in https://cai.inria.fr/wp-admin/customize.php >> CSSadditionnel
   * `@import url("https://files.inria.fr/mecsci/wp_plugin_cai/css/style-cai.css");`   (or any other suitable location)

=== On going development ===

Please refer to our [agile development pad](https://pad.inria.fr/p/np_D13sp24NPvFr1KLc_cai.inria.fr)

=== Installed plugin ===

* __Theme__
   * [femina](https://fr.wordpress.org/themes/femina) parent theme to be installed via, e.g., https://$wordpress-address/wp-admin/themes-install.php searching for existing themes.
   * [femina-cai](https://gitlab.inria.fr/line/wp_theme_cai/-/archive/master/wp_theme_cai-master.zip) children via, e.g., https://$wordpress-address/wp-admin/themes-install.php => [Upload a theme] since available here a zip file, to be downlaod on this link.

Plugins are installed via https://$wordpress-address/wp-admin/plugin-install.php

* __General functionalities__
   * [adminimize](https://fr.wordpress.org/plugins/adminimize) to hidden some useless options to editors and others, to lighten the interface
   * [broken-link-checker](https://fr.wordpress.org/plugins/broken-link-checker) to automatically detect broken-links
   * [custom-sidebars](https://fr.wordpress.org/plugins/custom-sidebars) to allow using different side-bars depending on the page context
   * [goolytics](https://fr.wordpress.org/plugins/goolytics-simple-google-analytics) to link with https://analytics.google.com/
   * [http-syndication](https://fr.wordpress.org/plugins/http-syndication) to import pages in our pages and posts
   * [remove-dashboard-access](https://fr.wordpress.org/plugins/remove-dashboard-access-for-non-admins) to avoid showing the admin bar when connected
   * [wp-external-links](https://fr.wordpress.org/plugins/wp-external-links) to open external links in a new tab by default

Other small functionality plugin include `classic-editor`, `duplicate-post`, `enable-media-replace`, `remove-dashboard-access-for-non-admins`, `wordfence` (security manager), and some plugins for multisite management `multisite-cloner`, `multisite-enhancements`, `network-subsite-user-registration`, `wordpress-mu-domain-mapping`, `wpmuldap`.

* __Content management__
   * [collapsing-categories](https://fr.wordpress.org/plugins/collapsing-categories) to manage category menu
   * [favorites](https://fr.wordpress.org/plugins/favorites) to manage book-marking of ressources
   * [pods](https://fr.wordpress.org/plugins/pods) to manage custom content types and fields, including display
   * [taxonomy-terms-order](https://fr.wordpress.org/plugins/taxonomy-terms-order) to sort all taxonomies
   * [post-types-order](https://fr.wordpress.org/plugins/post-types-order) to sort all posts
   * [sassy-social-share](https://fr.wordpress.org/plugins/sassy-social-share) to share contents on social networks

* __User interaction__
   * [asgaros-forum](https://fr.wordpress.org/plugins/asgaros-forum) our forum and dialog main tool
   * [formidable](https://fr.wordpress.org/plugins/formidable) to manage forms and user input
   * [ultimate-member](https://fr.wordpress.org/plugins/ultimate-member) to manage user profile, extra meta-fields, and profile display
   * [custom-twitter-feeds](https://fr.wordpress.org/plugins/custom-twitter-feeds) to displays the ;ast tweets

* __Event manager__
   * [events-manager](https://fr.wordpress.org/plugins/events-manager) to manage rendez-vous and other events
   * [stonehenge-em-osm](https://fr.wordpress.org/plugins/stonehenge-em-osm) to link events-manager with open street-map
      * + requires the PHP [intl](https://www.php.net/manual/fr/book.intl.php) extension installed.

* __Development tools__
   * [ad-inserted](https://fr.wordpress.org/plugins/ad-inserter) to allow adding JS or PHP middleware elements

== Frequently Asked Questions ==

== Changelog ==

= 0.0 =

== Other notes ==

