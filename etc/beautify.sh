#
# This script normalizes the style.css file
#
# Install tool using : sudo npm install -g cssbeautify-cli
#

ROOT="`realpath $0`" ; ROOT="`dirname $ROOT`" ; ROOT="`dirname $ROOT`" ; cd $ROOT

mv style.css etc/style.css.save
cssbeautify-cli -f etc/style.css.save -w style.css
