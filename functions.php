<?php
add_action('wp_enqueue_scripts', 'wp_theme_cai_enqueue_styles');

function wp_theme_cai_enqueue_styles() {
  // Register the child theme style.css after the parent style femina-style
  wp_enqueue_style( 'cai-style', get_stylesheet_uri(),
                     array( 'femina-style' ) ,
                     wp_get_theme()->get('Version') // this only works if you have Version in the style header
                  );
}


/**
* Allow Pods Templates to use shortcodes
*
* NOTE: Will only work if the constant PODS_SHORTCODE_ALLOW_SUB_SHORTCODES is defined and set to
  true, which by default it IS NOT.
*/
add_filter( 'pods_shortcode', function( $tags )  {
  $tags[ 'shortcodes' ] = true;
  return $tags;
});

/*Adding clear:both div after the_content */
add_filter( 'the_content', 'add_clear_to_the_content_in_the_main_loop' );
 function add_clear_to_the_content_in_the_main_loop( $content ) {
    // Check if we're inside the main loop in a single post page.
    if ( is_single() && in_the_loop() && is_main_query() ) {
        return $content .'<div class="clear"></div>';
    }
 
    return $content;
}

/*Adding custom type to category search*/
function namespace_add_custom_types( $query ) {
  if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
    $query->set( 'post_type', array(
			   'post', 'nav_menu_item', 'ressource', 'breve', 'projet'));
	  return $query;
	}
}
add_filter( 'pre_get_posts', 'namespace_add_custom_types' );


/**
* Customization of the Tag Cloud min and max font-size
*
*/
add_filter( 'widget_tag_cloud_args', 'change_tag_cloud_font_sizes');
/**
 * Change the Tag Cloud's Font Sizes.
 *
 * @since 1.0.0
 *
 * @param array $args
 *
 * @return array
 */
function change_tag_cloud_font_sizes( array $args ) {
    $args['smallest'] = '8';
    $args['largest'] = '16';

    return $args;
}