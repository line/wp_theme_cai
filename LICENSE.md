This is free software and resource, original files are licensed under:

* [CeCILL-C](https://cecill.info/licences/Licence_CeCILL-C_V1-fr.html) for the code.
* [CC-BY](https://creativecommons.org/licenses/by/3.0/fr/) for the contents.
